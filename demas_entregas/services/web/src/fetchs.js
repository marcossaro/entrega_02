export function crearPartida(id, nombre, key, fecha, puntos, muertes, tiempo){
    return fetch('https://server-entrega-02.glitch.me',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body :
            JSON.stringify({
              query: `mutation {crearPartida(id: "${id}", nombre: "${nombre}", key: "${key}", fechaHora: "${fecha}", puntos: "${puntos}", muertes:"${muertes}", tiempo:"${tiempo}")}`
            })
        })
        .then(response => response.json())
        .catch(console.log("error"))
}

export function mostrarPartida(id){
    return fetch('https://server-entrega-02.glitch.me',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body :
            JSON.stringify({
              query: `query { partida (id: "${id}"){id, nombre, fechaHora, puntos, muertes, tiempo, record}}`
            })
        })
        .then(response => response.json())
        .catch(console.log("error"))
}