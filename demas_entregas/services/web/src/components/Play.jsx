import React, { useContext } from "react";
import AuthContext from "../AuthContext.js";
import {crearPartida} from '../fetchs.js'
import { v4 as uuidv4 } from 'uuid';
import styles from "../forms.css";


const Play = () => {
  const { user } = useContext(AuthContext);

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  const crear = () => {
    const fecha = new Date().toLocaleString();;
    crearPartida(uuidv4(), user.name, user.key, fecha, getRandomInt(100, 1000), getRandomInt(0, 50), getRandomInt(1, 200))
    .then((response) => console.log(response))
  }

  return (
    <div>
      <div className={styles.cuadrado}>
        <div className={styles.center}>
          <p>- JUEGO - </p>
        </div>
      </div>
      <button onClick={crear}>Crear Partida</button>
    </div>
      
    );
};

export default Play;
