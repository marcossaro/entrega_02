const { GraphQLServer } = require("graphql-yoga");
// `fetch` es una API de navegador. Por lo tanto, no está disponible en NodeJS. El paquete `node-fetch` implementa un clon de `fetch` utilizable en NodeJS.
const fetch = require("node-fetch");

import fetchs from "./fetchs.js";

//const uuid = require('uuid/v4');

// Los puntos suspensivos en la siguiente función son los operadores rest y spread, respectivamente. Más info.: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters, https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
const fetchJson = (...args) => fetch(...args).then(response => response.json());

const schema = `
  type Query {
    partidas(id: ID!): Partidas
  }
  type Mutation {
    crearPartida(nombre: String!): Partidas!
    cerrarPartida(id: ID!): Partidas!
  }
  type Partidas {
    id: ID!
    nombre: String!
    datos : [Estadisticas!]!
    estado : Boolean!
    fecha : String!
    hora : String!
  }
  type Estadisticas {
    id: ID!
    puntos: String!
    muertes: String!
    tiempo: String!
  }

`;

const resolvers = {
  Query: {
    partidas: (_, { id }) => fetchs.readAllPair
  },
  Mutation: {
    crearPartida : async ({id}, args, context) => {
      const event = fetchs.createPair(id, body);

      return event.partidas;
    }  
	},  
};

const server = new GraphQLServer({ 
  typeDefs: schema, 
  resolvers,
});

server.start({
  playground: "/",
  port: 3100
});


/*
const { GraphQLServer } = require("graphql-yoga");
const fetch = require("node-fetch");

const fetchJson = (...args) => fetch(...args).then(response => response.json());

const schema = `
  type Query {
    pokemon(id: ID!): Pokemon
  }
  type Mutation {
    createPokemon(name: String!): Pokemon!
  }
  type Pokemon {
    id: ID!
    name: String!
    types: [Type!]!
  }
  type Type {
    id: ID!
    name: String!
    pokemons(offset: Int!, first: Int!): [Pokemon!]!
  }
`;

const BASE_URL = "https://pokeapi.co/api/v2";

const resolvers = {
  Query: {
    pokemon: (_, { id }) => fetchJson(`${BASE_URL}/pokemon/${id}/`)
  },
  Mutation: {
    createPokemon: (_, args) => {
      console.log("pokemon created with args:", args);
      return { id: 0, name: args.name, types: [], locations: [] };
    }
  },
  Pokemon: {
    id: pokemon => pokemon.id,
    name: pokemon => pokemon.name,
    types: pokemon =>
      pokemon.types.map(({ type }) => type.url).map(url => fetchJson(url))
  },
  Type: {
    id: type => type.id,
    name: type => type.name,
    pokemons: (type, args) =>
      type.pokemon
        .slice(args.offset, args.first + args.offset)
        .map(({ pokemon }) => fetchJson(pokemon.url))
  }
};

const server = new GraphQLServer({ 
  typeDefs: schema, 
  resolvers,
});

server.start({
  playground: "/",
  port: process.env.PORT
});

*/