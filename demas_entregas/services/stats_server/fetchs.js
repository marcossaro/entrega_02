const fetch = require('node-fetch');
const {x_application_id} = 'd3d2a595-389b-43e2-a535-0ba7a480f57c';


const url = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";

/*
    A desarrollar 4 fetch ..

    1- Create a pair (put)

    all get down here ...
    2- Read a pair
    3- Read all pairs
    4- Read pairs by key prefix

key = 8e396b2f-2a39-447c-8317-3bae6a4a22ff

 */


/* 1- CREATE A PAIR

    Este es usado para mandar data y almacenarla como su nombre indica y lo usaremos para almacenar data
    y clasificada por el key

    PUT /pairs/8e396b2f-2a39-447c-8317-3bae6a4a22ff HTTP/1.1
    [...]
    x-application-id: 2710638a-a986-442f-b029-feb40bd4d4dd

    {
        id: [...],
        title: [...],
        [...]
    }

 */
const createAPair = (id, body) => {

    // pokemon: (_, {id}) => fetch(`${BASE_URL}/pokemon/${id}/`).then(res => res.json())
    // PUT /pairs/8e396b2f-2a39-447c-8317-3bae6a4a22ff HTTP/1.1
    fetch(`${url}/pairs/${id}`,
        {
            method: 'PUT',
            headers: {"x-application-id": x_application_id},
            body: JSON.stringify(body)
        })
        .then(response => response.json())
        .catch(Console.log("error"));

}

/* 2- READ A PAIR

    Devuelve un pair y  este es el valor para la clave que le metes

    GET /pairs/8e396b2f-2a39-447c-8317-3bae6a4a22ff HTTP/1.1
    [...]

    x-application-id: 2710638a-a986-442f-b029-feb40bd4d4dd

 */
const readPair = (id) => {

    return fetch(`${url}/pairs/${id}`,
        {
            method: 'GET',
            headers: {"x-application-id": x_application_id}
        })
        .then(response => response)
        .then(response => response.json())
        .catch(Console.log("error"));
}

/* 3- READ ALL PAIRS

    Te devolvera todos los pair sin importar el key... no hay filtro devuelve todos..

    GET /pairs HTTP/1.1
    [...]
    x-application-id: 2710638a-a986-442f-b029-feb40bd4d4dd

 */
const readAllPair = () => {

    return fetch(`${url}/pairs`,
        {
            method: 'GET',
            headers: {"x-application-id": x_application_id}
        })
        .then(response => response.json())
        .catch(Console.log("error"));

}

/* 4- READ PAIRS BY KEY PREFIX

    Utilizada para leer data filtrandolo por el key ... devuelve todos los que tengan ese key

    GET /collections/prefix- HTTP/1.1
    [...]
    x-application-id: 2710638a-a986-442f-b029-feb40bd4d4dd

 */
const readPairsByKeyPrefix = (id) => {
    const url = `${url}/collections/${id}`;

    return fetch(url,
        {
            method: 'GET',
            headers: {"x-application-id": x_application_id}
        })
        .then(response => response.json())
        .catch(Console.log("error"));

}

module.exports={createAPair,readPair,readAllPair,readPairsByKeyPrefix}